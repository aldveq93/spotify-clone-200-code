import React, { useState, useEffect } from "react";
import { useStore } from "../../utilities/hooks/use-store";
import { Menu } from "../../components";

const PlayLists = () => {
  const [playlists, setPlaylist] = useState([]);
  const [loading, setLoading] = useState(true);
  const { service } = useStore();

  // Cuando se monta
  // Cuando está montado y cambió
  // Cuando se desmontó
  useEffect(() => {
    const getPlaylists = async () => {
      const data = await service.getPlaylists();
      setPlaylist(data.items);
      setLoading(false);
    };
    getPlaylists();
  }, []);

  if (loading) {
    return <p style={{ color: "#fff" }}>Loading playlists...</p>;
  }

  return (
    <Menu title="Playlists">
      {playlists.map((playlist) => (
        <Menu.Item
          key={playlist.id}
          to={`/users/${playlist.owner.id}/playlist/${playlist.id}`}
        >
          {playlist.name}
        </Menu.Item>
      ))}
    </Menu>
  );
};

export default PlayLists;
