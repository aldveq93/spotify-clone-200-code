import React from "react";
import "./styles.css";

export const GeneralListItem = ({ children }) => {
  return <li className="general-list-item">{children}</li>;
};

const GeneralList = ({ title, children }) => {
  return (
    <div>
      <h1 className="general-list-title">{title}</h1>
      <ul className="general-list-container">{children}</ul>
    </div>
  );
};

GeneralList.Item = GeneralListItem;

export default GeneralList;
