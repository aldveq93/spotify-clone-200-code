const getLS = () => {
  const storedInfo = JSON.parse(localStorage.getItem("sp_token"));
  return storedInfo;
};

export const getToken = () => {
  const storedToken = getLS();
  return { token: storedToken.access_token };
};
